import math
def factorial(number):
    # Votre code ici
    facto = math.factorial(abs(number))
    return facto if number >= 0 else facto * -1

def run():
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(4) == 24
    assert factorial(8) == 40320
    assert factorial(-8) == -40320