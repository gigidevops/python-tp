# import math
def square(number):
    # Votre code ici

    # print("Saississez un nombre {number}")
    # ma_racine = math.sqrt(number)
    # return ma_racine
    return number*number


def run():
    assert square(1) == 1
    assert square(2) == 4
    assert square(3) == 9
    assert square(23) == 529
    assert square(-23) == 529
