def is_number_correct(number):
    # Votre code ici
    correct = 10 <= number <= 20
    if number < 10:
       return (correct, 10 - number)
    elif number > 20:
        return (correct, 20 - number)
    else:
        return (correct, 0)

def run():
    assert is_number_correct(0) == (False, 10)
    assert is_number_correct(10) == (True, 0)
    assert is_number_correct(20) == (True, 0)
    assert is_number_correct(21) == (False, -1)
    assert is_number_correct(50) == (False, -30)
    assert is_number_correct(15) == (True, 0)
